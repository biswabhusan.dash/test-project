Top screen resolution used list (up to may 2020)
Sl No Resolution Worldwide Used (%)

1.      1920 x 1080	8.13
2.      1600 x 900	1.77
3.      1536 x 864 	2.66
4.      1440 x 900 	2.67
5.      1366 x 768	8.94
6.      1280 x 800	1.61
7.      1024 x 768	1.29
8.      768 x 1024	2.52
9.      720 x 1280	2.23
10.     412 x 846		2.27
11.     414 x 736		2.33
12.     393 x 786 	2.29
13.     375 x 667 	4.51
14.     360 x 640		14.06
15.     320 x 570		1.4
16.     Other				41.32

Top browsers used list (up to may 2020)

Sl No Name Worldwide Used (%)

1.      Chrome						63.91
2.      Safari						18.2
3.      UC Browser				2
4.      Firefox						4.39
5.      Opera							1.91
6.      IE								1.4
7.      Samsung Internet	3.28
8.      Android						0.54
9.      Edge							2.13
10.     Other							2.24

Referances –
• https://gs.statcounter.com/
• https://www.w3schools.com/browsers/browsers_display.asp
