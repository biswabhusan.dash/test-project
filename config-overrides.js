const {
	override,
	fixBabelImports,
	addLessLoader,
	addWebpackAlias,
} = require('customize-cra');

module.exports = override(
	fixBabelImports('import', {
		libraryName: 'antd',
		libraryDirectory: 'es',
		style: true,
	}),
	addLessLoader({
		javascriptEnabled: true,
		modifyVars: { '@primary-color': '#f58740' },
	}),

	//Added addWebpackAlias for enabling react app profiling in production build.
	//Reference 1- https://reactjs.org/blog/2018/09/10/introducing-the-react-profiler.html
	//Reference 2- https://gist.github.com/bvaughn/25e6233aeb1b4f0cdb8d8366e54a3977
	addWebpackAlias({
		'react-dom$': 'react-dom/profiling',
		'scheduler/tracing': 'scheduler/tracing-profiling',
	})
);
