// import style files start
import './styles/style.less';
// import style files end
import React from 'react';
import {
	AntButton,
} from '@datoms/react-components';

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		return <AntButton>Submit</AntButton>;
	}
}

export default App;