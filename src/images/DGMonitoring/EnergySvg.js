import React from 'react';

export default class RunhourSvg extends React.Component {
	render() {
		return (
			<svg
				xmlns="http://www.w3.org/2000/svg"
				width="30.83"
				height="30.338"
				viewBox="0 0 11.972 18.625"
			>
				<g
					id="Group_150"
					data-name="Group 150"
					transform="translate(-844.453 -442.458)"
				>
					<g
						id="Group_149"
						data-name="Group 149"
						transform="translate(849.752 458.269)"
					>
						<text
							id="Energy"
							transform="translate(0 1)"
							fill="#fff"
							font-size="1"
							font-family="MyriadPro-Regular, Myriad Pro"
							letter-spacing="0.061em"
						>
							<tspan x="0" y="0">
								Energy
							</tspan>
						</text>
					</g>
					<path
						id="Path_92"
						data-name="Path 92"
						d="M849.613,451.509H846.6c.051-.1.077-.167.116-.232,1.429-2.717,2.858-5.446,4.287-8.163.051-.116.1-.206.257-.206,1.429.013,2.845,0,4.275,0a.631.631,0,0,1,.142.013c-1.339,2.009-2.652,3.991-4,6h3.837c.013.013.026.039.039.051q-5.253,5.736-10.506,11.459c-.013-.013-.026-.013-.039-.026C846.536,457.457,848.055,454.509,849.613,451.509Z"
						fill="none"
						stroke="#45cdc3"
						stroke-miterlimit="10"
						stroke-width="0.9"
					/>
				</g>
			</svg>
		);
	}
}
