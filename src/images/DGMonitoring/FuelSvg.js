import React from 'react';

export default class RunhourSvg extends React.Component {
	render() {
		return (
			<svg
				xmlns="http://www.w3.org/2000/svg"
				width="30.83"
				height="30.338"
				viewBox="0 0 21.799 15.031"
			>
				<g
					id="Group_143"
					data-name="Group 143"
					transform="translate(-95.8 -465.251)"
				>
					<path
						id="Path_74"
						data-name="Path 74"
						d="M104.21,470.9a1.788,1.788,0,0,1,1.378.535,1.745,1.745,0,0,1,.454,1.212c0,1.165-.016,2.346,0,3.511a1.28,1.28,0,0,0,.939,1.275,1.169,1.169,0,0,0,1.393-.645,1.68,1.68,0,0,0,.11-.913,27.953,27.953,0,0,1-.986-5.321,5.681,5.681,0,0,0-1.941-3.967c-.078-.063-.188-.126-.2-.2-.047-.252-.063-.52-.094-.772a1.694,1.694,0,0,1,.564,0,5.5,5.5,0,0,1,3.131,3.416c.078.2.141.425.219.63.016.032,0,.079,0,.126a.416.416,0,0,1-.094.079c-.673.283-.72.3-.6,1.008.282,1.574.6,3.133.955,4.692a2.237,2.237,0,0,1-1.675,2.834,2.175,2.175,0,0,1-2.615-2.062c-.031-1.2,0-2.393-.016-3.59,0-.6-.282-.866-.924-.866v6.581c0,.346.047.551.454.551.376,0,.266.346.282.583.016.22-.016.425-.313.441H96.663a.566.566,0,0,1-.5-.9.371.371,0,0,1,.235-.142c.438.031.454-.22.454-.551v-11.9c0-.74.313-1.039,1.033-1.039h5.308c.7,0,1.018.315,1.018,1.023v4.093A.571.571,0,0,0,104.21,470.9Zm-3.695-4.408H98.229c-.282,0-.407.11-.407.394v3.464c0,.283.125.394.407.394H102.8c.3,0,.423-.11.423-.425-.016-1.134-.016-2.283,0-3.416,0-.315-.125-.409-.423-.409Z"
						transform="translate(0 0)"
						fill="none"
						stroke="#45cdc3"
						stroke-miterlimit="10"
						stroke-width="0.5"
					/>
					<g
						id="Group_143-2"
						data-name="Group 143"
						transform="translate(110.976 465.501)"
					>
						<path
							id="Path_75"
							data-name="Path 75"
							d="M234.764,466.3v-.8h6.623v.8Z"
							transform="translate(-234.764 -465.501)"
							fill="#45cdc3"
						/>
						<path
							id="Path_76"
							data-name="Path 76"
							d="M240.358,481.49v.787h-5.594v-.787Z"
							transform="translate(-234.764 -479.77)"
							fill="#45cdc3"
						/>
						<path
							id="Path_77"
							data-name="Path 77"
							d="M234.764,498.371v-.787h5v.787Z"
							transform="translate(-234.764 -494.133)"
							fill="#45cdc3"
						/>
						<path
							id="Path_78"
							data-name="Path 78"
							d="M234.764,513.365h4.465v.809h-4.465Z"
							transform="translate(-234.764 -508.217)"
							fill="#45cdc3"
						/>
						<path
							id="Path_79"
							data-name="Path 79"
							d="M234.764,530.257v-.8h3.927v.8Z"
							transform="translate(-234.764 -522.58)"
							fill="#45cdc3"
						/>
						<path
							id="Path_80"
							data-name="Path 80"
							d="M238.207,546.34h-3.443v-.787h3.443Z"
							transform="translate(-234.764 -536.943)"
							fill="#45cdc3"
						/>
						<path
							id="Path_81"
							data-name="Path 81"
							d="M237.386,561.542v.776h-2.622v-.776Z"
							transform="translate(-234.764 -551.212)"
							fill="#45cdc3"
						/>
						<path
							id="Path_82"
							data-name="Path 82"
							d="M236.42,577.323v.8h-1.656v-.8Z"
							transform="translate(-234.764 -565.296)"
							fill="#45cdc3"
						/>
						<path
							id="Path_83"
							data-name="Path 83"
							d="M235.689,594.308h-.925v-.787h.925Z"
							transform="translate(-234.764 -579.752)"
							fill="#45cdc3"
						/>
					</g>
				</g>
			</svg>
		);
	}
}
