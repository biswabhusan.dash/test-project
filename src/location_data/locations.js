/**
 * 	Locations of the World.
 * 	{
 * 		"India": {
 * 			"Odisha": [
 * 				"Bhubaneswar",
 * 				"Rourkela",
 * 				...
 * 			],
 * 			...
 * 		},
 * 		...
 * 	}
 */

const locations = require('./locations.json');

let countries = {};
if (locations && locations.length) {
	locations.map((country) => {
		countries[country.name] = country.states;
	});
}

module.exports = countries;
